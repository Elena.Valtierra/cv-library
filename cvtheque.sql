CREATE TABLE Users (
	idUser integer primary key auto_increment,  
    nom varchar(100),
    prenom varchar(100),
    age varchar(100),
    pays varchar(100),
    ville varchar(100),
    codePostal integer(5),
    nad  integer(5),
    voie varchar(100),
    telephone integer(10)
    );


CREATE TABLE Connexion (
    idConnexion integer primary key auto_increment,  
    is_admin boolean, 
    username varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    mp  varchar(100) NOT NULL,
    idUser integer, foreign key(IdUser) references Users(IdUser)
);

CREATE TABLE Formation (
	idFormation integer primary key auto_increment,  
    intitule varchar(100),
    dateDebut DATE,
    dateFin DATE,
    organisme varchar(100)
    );

CREATE TABLE Competences (
	idCompetence integer primary key auto_increment,  
    intitule varchar(100),
    niveau varchar(50)
    );

CREATE TABLE UserCompetence (
	idUserComp integer primary key auto_increment,  
    idUser integer, foreign key(IdUser) references Users(IdUser),
    idCompetence integer, foreign key(idCompetence) references Competences(idCompetence)
    );

CREATE TABLE Hobbies (
	idHobby integer primary key auto_increment,  
    intitule varchar(100),
    descrip varchar(500),
    idUser integer, foreign key(IdUser) references Users(IdUser)
    );

CREATE TABLE Reseaux (
	idContact integer primary key auto_increment,  
    intitule varchar(100),
    lien varchar(100),
    idUser integer, foreign key(IdUser) references Users(IdUser)
);


CREATE TABLE Experiences (
  idExpe integer(11) primary key auto_increment,  
  intitule varchar(255),
  datedebut date,
  datefin date,
  descrip varchar(255),
  structure varchar(100),
  idUser integer(11), foreign key(IdUser) references Users(IdUser)  
);



