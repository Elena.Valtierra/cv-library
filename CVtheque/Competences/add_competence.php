

<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>

<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
Ajouter Compétence
</title>

</head>

<body>
    <h1>AJOUTER UNE COMPETENCE</h1>
    <hr></hr>

<form name="myForm" action="add_competence_save.php" method="post">
<table border="2" >
<tr>
        <td colspan="2">
        <a href="add_competence.php">Nouvelle competence</a>
    </td>
</tr>
<tr>
<td>
    Intitulé
</td>
<td>
    <input type="text" name="intit" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Niveau
</td>
<td>
    <input type="text" name="niv" value="" required="yes">
</td>
</tr>

<tr>

<td colspan="2">
    <input type="submit" value="Enregistrer">
</td>
</tr>
</table>
</form>
</body>
</html>