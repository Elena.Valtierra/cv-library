

<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>

<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
Ajouter Formation
</title>

</head>

<body>
    <h1>AJOUTER UNE FORMATION</h1>
    <hr></hr>

<form name="myForm" action="add_formation_save.php" method="post">
<table border="2" >
<tr>
        <td colspan="2">
        <a href="add_experience.php">Nouvelle formation</a>|<a href="listeFormations.php">Liste des formations </a>
    </td>
</tr>
<tr>
<td>
    Intitulé
</td>
<td>
    <input type="text" name="intit" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Date de début
</td>
<td>
    <input type="text" name="datedebut" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Date de fin
</td>
<td>
    <input type="text" name="datefin" value="" required="yes">
</td>
</tr>

<tr>
<td>
    Organisme
</td>
<td>
    <input type="text" name="organisme" value="" required="yes">
</td>
</tr>


<tr>

<td colspan="2">
    <input type="submit" value="Enregistrer">
</td>
</tr>
</table>
</form>
<a href="../authentification/index.php">Retour à l'espace personnel</a>
</body>
</html>