

<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
    }
 
?>

<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
Ajouter un Hobbie
</title>

</head>

<body>
    <h1>AJOUTER UN HOBBIES</h1>
    
    <hr></hr>

<form name="myForm" action="add_hobbies_save.php" method="post">
<table border="2" >
<tr>
        <td colspan="2">
        <a href="add_hobbies.php">Nouvel hobbie</a>
        <a href="listeHobbies.php">List hobbies</a>
    </td>
    
</tr>
<tr>
<td>
    Intitulé
</td>
<td>
    <input type="text" name="int" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Description
</td>
<td>
    <input type="text" name="description" value="" required="yes">
</td>
</tr>

<td colspan="2">
    <input type="submit" value="Enregistrer">
</td>
</tr>

</table>
</form>

<a href="../authentification/index.php">Retour à l'espace personnel</a>
</body>
</html>