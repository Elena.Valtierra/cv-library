﻿<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: login.php");
		exit(); 
	}
?>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="style.css" />
	<link rel="stylesheet" href="navbar.css" />
	</head>
	<body>
		<div class="sucess">
		<h1>Bienvenue <?php echo $_SESSION['username']; ?>!</h1>
		
		<br>
	<h1 class="box-title">ESPACE PERSONNEL</h1>
	<div class="navbar">
  <a href="../index2.php">Accueil</a>
  <a href="../connexion/update_connexion.php">Modifier mon profil</a>             
  <div class="dropdown">
    <button class="dropbtn">Mettre à jour mon CV 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="../formations/add_formation.php">Formations</a>
      <a href="../experiences/add_experience.php">Experiences</a>
	  <a href="../hobbies/add_hobbies.php">Hobbies</a>
	  <a href="../Reseaux/add_reseaux.php">Réseaux</a>
	  <a href="../ex/add_experience.php">Competences</a>
	  
    </div>
  </div> 

  <div>

        <a href="../Users/add_user.php">Compléter votre profil</a>
        </div>
   <a href="logout.php" >Deconnexion</a>
</div>
		
		
		
		</div>
		
		
		
		
	</body>
</html>