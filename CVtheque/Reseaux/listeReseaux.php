
<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>
<?php
 
 include("connexion_db.php");


// connect to the database
//include('connect-db.php');


// Create connection
$conn=mysqli_connect($server, $user, $mp, $databasename);
$sql = "SELECT * FROM reseaux";

// Connexion à la database
if ($result=mysqli_query($conn, $sql))
{
// Affichage des résultats
   if ($result->num_rows > 0)
    {
          // Affichage d'un tableau
         echo "<table border='10' cellpadding='10'>";

// set table headers
    echo "<tr><th>IDcontact</th><th>Contact</th><th>Intitulé</th><th>Lieu</th></tr>";

while ($row = $result->fetch_object())
{
// set up a row for each record
echo "<tr>";

echo "<td>" . $row->idContact . "</td>";
echo "<td>" . $row->intitule . "</td>";
echo "<td>" . $row->lien . "</td>";

// il y a deux ID on veux que avec le deux on puisse modifier et supprimer

echo "<td><a href='update_reseaux.php?id=". $row->idContact. "'>Modifier</a></td>";

echo "<td><a href='delete_reseaux.php?id=" . $row->idContact. "'>Supprimer</a></td>";
echo "</tr>";
}

echo "</table>";
}
// if there are no records in the database, display an alert message
else
{
echo "Pas de résultats";
}
}
// show an error if there is an issue with the database query
else
{
echo "Error: " . $mysqli->error;
}

// close database connection
//$mysqli->close();
mysqli_close($conn);
?>




<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
    Liste Réseaux
</title>

</head>


<body>
    <a href="add_reseaux.php">Nouveau réseau</a>
    <a href="../authentification/index.php">Retour à l'espace personnel</a>
    include("phpToPDF.php");


</body>
</html>

