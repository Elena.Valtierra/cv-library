

<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>

<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
Ajouter un réseau
</title>

</head>

<body>
    <h1>AJOUTER UN RESEAU</h1>
    <hr></hr>

<form name="myForm" action="add_reseaux_save.php" method="post">
<table border="2" >
<tr>
        <td colspan="2">
        <a href="add_reseaux.php">Nouveau Réseau</a>|<a href="listeReseaux.php">Liste des réseaux </a>
    </td>
</tr>
<tr>
<td>
    Intitulé
</td>
<td>
    <input type="text" name="int" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Lien
</td>
<td>
    <input type="text" name="lien" value="" required="yes">
</td>
</tr>

<td colspan="2">
    <input type="submit" value="Enregistrer">
</td>
</tr>

</table>
</form>
<a href="../authentification/index.php">Retour à l'espace personnel</a>
</body>
</html>