
<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>
<?php
 
 include("connexion_db.php");


// connect to the database
//include('connect-db.php');


// Create connection
$conn=mysqli_connect($server, $user, $mp, $databasename);
$sql = "SELECT * FROM users";

// Connexion à la database
if ($result=mysqli_query($conn, $sql))
{
// Affichage des résultats
   if ($result->num_rows > 0)
    {
          // Affichage d'un tableau
         echo "<table border='10' cellpadding='10'>";

// set table headers
    echo "<tr><th>ID</th><th>NOM</th><th>PRENOM</th><th>AGE</th><th>PAYS</th><th>Ville</th></tr>";

while ($row = $result->fetch_object())
{
// set up a row for each record
echo "<tr>";
echo "<td>" . $row->idUser . "</td>";
echo "<td>" . $row->nom . "</td>";
echo "<td>" . $row->prenom . "</td>";
echo "<td>" . $row->age . "</td>";
echo "<td>" . $row->pays . "</td>";
echo "<td>" . $row->ville . "</td>";
echo "<td>" . $row->codePostal . "</td>";
echo "<td>" . $row->nad . "</td>";
echo "<td>" . $row->voie . "</td>";
echo "<td>" . $row->téléphone . "</td>";
echo "<td><a href='update_user.php?id=". $row->idUser . "'>Modifier</a></td>";
echo "<td><a href='delete_user.php?id=" . $row->idUser . "'>Supprimer</a></td>";
echo "</tr>";
}

echo "</table>";
}
// if there are no records in the database, display an alert message
else
{
echo "Pas de résultats";
}
}
// show an error if there is an issue with the database query
else
{
echo "Error: " . $mysqli->error;
}

// close database connection
//$mysqli->close();
mysqli_close($conn);
?>




<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
    Liste utilisateurs
</title>

</head>


<body>
    <a href="add_user.php">Nouveau utilisateur</a>
    <a href="../authentification/index.php">Retour à l'espace personnel</a>

</body>
</html>