

<?php
	// Initialiser la session
	session_start();
	// Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
	if(!isset($_SESSION["username"])){
		header("Location: ../auth/login.php");
		exit(); 
	}
?>

<html>
<head>
    <link rel="stylesheet" href="..\Assets\Style.css">


<title>
Ajouter un utilisateur
</title>

</head>

<body>
    <h1>AJOUTER UN UTILISATEUR</h1>
    <hr></hr>

<form name="myForm" action="add_user_save.php" method="post">
<table border="2" >
<tr>
        <td colspan="2">
        <a href="add_user.php">Nouvel utilisateur</a>
    </td>
</tr>
<tr>
<td>
    Nom
</td>
<td>
    <input type="text" name="nme" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Prénom
</td>
<td>
    <input type="text" name="pne" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Age
</td>
<td>
    <input type="number" name="age" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Pays
</td>
<td>
    <input type="text" name="pays" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Ville
</td>
<td>
    <input type="text" name="ville" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Code Postal
</td>
<td>
    <input type="number" name="cp" value="" required="yes">
</td>
</tr>
<tr>
<td>
    N° de la voie
</td>
<td>
    <input type="number" name="nad" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Voie
</td>
<td>
    <input type="text" name="voie" value="" required="yes">
</td>
</tr>
<tr>
<td>
    Téléphone
</td>
<td>
    <input type="number" name="tel" value="" required="yes">
</td>
</tr>
<tr>

<td colspan="2">
    <input type="submit" value="Enregistrer">
</td>
</tr>
</table>
</form>
<a href="../authentification/index.php">Retour à l'espace personnel</a>
</body>
</html>